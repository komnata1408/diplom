﻿using System;
using System.Collections.Generic;
using AlgorithmDLL;
using System.Text;
using System.IO;
using System.Linq;

namespace TestDLL
{
    class Program
    {
        static int TilesLenght(List<string> tiles)
        {
            int rez = 0;
            foreach (string str in tiles)
                rez += str.Length;
            return rez;
        }

        static public void CheckAndWrite(string path1, string path2, string lang, StreamWriter resultFile)
        {
            /*
            DateTime before = DateTime.Now;
            string str1 = new CodeToken(path1, "languages/", lang).ToString();
            string str2 = new CodeToken(path2, "languages/", lang).ToString();
            resultFile.WriteLine("lab1.Length = {0}", str1.Length);
            resultFile.WriteLine("lab2.Length = {0}", str2.Length);
            int rez = TilesLenght(Algorithms.GST(str1, str2));
            resultFile.WriteLine("GST(lab1, lab2) = {0} ({1}%)", rez, (int)Math.Round((double)rez * 2 / (double)(str1.Length + str2.Length) * (double)100));
            DateTime after = DateTime.Now;
            resultFile.WriteLine("time elapsed: {0}\n", after - before);
            */
        }

        static void Main(string[] args)
        {
            /*
            StreamWriter resultFile = new StreamWriter(File.Create("z_result_opt.txt"));

            int tests = 19;

            DateTime before = DateTime.Now;
            for (int i = 1; i <= tests; i++)
            {
                Console.Write("\rprocessing: {0} from {1}", i, tests);
                string path = "labs/TestSet/TestSet_SID/Method-Test" + i + "";
                var files = Directory.EnumerateFiles(path).ToList();
                CheckAndWrite(files[0], files[1], "java", resultFile);
            }
            DateTime after = DateTime.Now;

            resultFile.Write("time elapsed: {0}", after - before);
            resultFile.Close();
            //Console.WriteLine("\n\nDone");
            //Console.Write("time elapsed: {0}", after - before);
            //Console.ReadKey();
            StreamWriter resultFile = new StreamWriter(File.Create("z_result.txt"));
            DateTime before = DateTime.Now;
            string str1 = new CodeToken("labs/300_lines.cpp", "languages/", "cpp").ToString();
            string str2 = new CodeToken("labs/300_lines_2.cpp", "languages/", "cpp").ToString();
            var rez = Algorithms.GST(str1, str2);
            int rez2 = TilesLenght(rez);
            DateTime after = DateTime.Now;
            resultFile.WriteLine("{0}\n{1}", str1, str2);
            resultFile.WriteLine("lab1.Length = {0}", str1.Length);
            resultFile.WriteLine("lab2.Length = {0}", str2.Length);
            resultFile.WriteLine("GST(lab1, lab2) = {0} ({1}%)", rez2,
                (int)Math.Round((double)rez2 * 2 / (double)(str1.Length + str2.Length) * (double)100));
            resultFile.WriteLine("time elapsed: {0}\n", after - before);
            foreach (var tile in rez)
                resultFile.WriteLine(tile);
            resultFile.Close();
            */
        }
    }
}
