﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace AlgorithmDLL
{
    public class KeywordsLibrary
    {
        private Dictionary<string, List<string>> lib = null;
        private static KeywordsLibrary instance = null;
        private static object lockobj = new object();
        private KeywordsLibrary()
        {
            lib = new Dictionary<string, List<string>>();
        }

        public static KeywordsLibrary GetInstance()
        {
            if (instance == null)
            {
                lock (lockobj)
                {
                    if (instance == null)
                    {
                        instance = new KeywordsLibrary();
                    }
                }
            }
            return instance;
        }

        public List<string> GetKeywords(string path, string lang)
        {
            if (lib.ContainsKey(lang))
                return lib[lang];
            else
            {
                if (!File.Exists(Path.Combine(path, lang + ".txt")))
                    throw new FileNotFoundException("Keywords for \"" + lang + "\" are not found!");
                using (StreamReader guf = new StreamReader(Path.Combine(path, lang + ".txt"), Encoding.UTF8))
                {
                    List<string> tmp = new List<string>();
                    while (!guf.EndOfStream)
                    {
                        tmp.Add(guf.ReadLine());
                    }
                    lib.Add(lang, tmp);
                    return lib[lang];
                }
            }
        }
    }
}
