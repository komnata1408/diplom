﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AlgorithmDLL
{
    public static class Algorithms
    {
        public static List<string> GST(string input1, string input2, int MinMatchLength = 20)
        {
            StringBuilder str1 = new StringBuilder(input1);
            StringBuilder str2 = new StringBuilder(input2);
            bool[] mark1 = new bool[str1.Length];
            bool[] mark2 = new bool[str2.Length];
            List<Match> matches = new List<Match>();
            List<string> tiles = new List<string>();
            int maxMatch = 0;

            do
            {
                matches = new List<Match>();
                maxMatch = MinMatchLength;
                for (int p = 0; p < str1.Length; p++)
                {
                    for (int t = 0; t < str2.Length; t++)
                    {
                        int j = 0;
                        while (!mark1[p + j] && !mark2[t + j] && str1[p + j] == str2[t + j])
                        {
                            j++;
                            if ((p + j) >= str1.Length || (t + j) >= str2.Length)
                                break;
                        }

                        if (j == maxMatch)
                        {
                            matches.Add(new Match(p, t, j));
                        }
                        else if (j > maxMatch)
                        {
                            matches = new List<Match>();
                            matches.Add(new Match(p, t, j));
                            maxMatch = j;
                        }
                    }
                }
                foreach (var match in matches)
                {
                    if (!mark1[match.P + match.J - 1] && !mark2[match.T + match.J - 1])
                    {
                        tiles.Add(str1.ToString(match.P, match.J));
                        for (int i = 0; i < match.J; i++)
                        {
                            mark1[match.P + i] = true;
                            mark2[match.T + i] = true;
                        }
                    }
                }
            } while (maxMatch > MinMatchLength);
            return tiles;
        }

        public static int GST_percent(string input1, string input2, int MinMatchLength = 20)
        {
            List<string> tiles = Algorithms.GST(input1, input2, MinMatchLength);
            int rez = 0;
            foreach (string str in tiles)
                rez += str.Length;
            return (int)Math.Round((double)rez * 2 / (double)(input1.Length + input2.Length) * (double)100);
        }

        public static int LCS(string str1, string str2)
        {
            int answer = 0;
            int[,] matrix = new int[str2.Length + 1, str1.Length + 1];

            for (int i = 1; i <= str2.Length; i++)
            {
                for (int j = 1; j <= str1.Length; j++)
                {
                    if (str1[j - 1] == str2[i - 1])
                    {
                        matrix[i, j] = matrix[i - 1, j - 1] + 1;
                        if (matrix[i, j] > answer)
                        {
                            answer = matrix[i, j];
                        }
                    }
                }
            }
            return answer;
        }

        public static int LCS_percent(string str1, string str2)
        {
            return (int)Math.Round((double)LCS(str1, str2) / (double)Math.Min(str1.Length, str2.Length) * (double)100);
        }
    }

    internal struct Match
    {
        public int P;
        public int T;
        public int J;

        public Match(int p, int t, int j)
        {
            this.P = p;
            this.T = t;
            this.J = j;
        }
    }
}
