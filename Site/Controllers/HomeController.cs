﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MySql.Data.MySqlClient;
using Site.Models;

namespace Site.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(string authID, string authPass, bool isTeacher)
        {
            using (DbWorker db = new DbWorker())
            {
                MySqlDataReader data;
                if (isTeacher)
                {
                    data = db.Query("SELECT * FROM teachers WHERE teachers_id = \""+authID+"\" AND password = \""+authPass+"\";");
                    if (data.Read())
                    {
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket( 1,
                            data[0].ToString(),
                            DateTime.Now,
                            DateTime.Now.AddHours(30),
                            false,
                            "Teacher" //data[1].ToString()
                        );
                        Response.Cookies.Add(new HttpCookie("__AUTH_COOK", FormsAuthentication.Encrypt(ticket)));
                        return RedirectToAction("Index", "Teach");
                    }
                }
                else
                {
                    data = db.Query("SELECT * FROM students WHERE students_id = \""+authID+"\" AND password = \""+authPass+"\";");
                    if (data.Read())
                    {
                        FormsAuthenticationTicket ticket = new FormsAuthenticationTicket( 1,
                            data[0].ToString(),
                            DateTime.Now,
                            DateTime.Now.AddHours(30),
                            false,
                            "Student" //data[1].ToString()
                        );
                        Response.Cookies.Add(new HttpCookie("__AUTH_COOK", FormsAuthentication.Encrypt(ticket)));
                        return RedirectToAction("Index", "Stud");
                    }
                }
            }
            return RedirectToAction("Index", "Home");
        }
    }
}
