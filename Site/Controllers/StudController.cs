﻿using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Site.Models;

namespace Site.Controllers
{
    public class StudController : Controller
    {
        //
        // GET: /Stud/

        public ActionResult Index()
        {
            if ((FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value)).UserData != "Student")
                return RedirectToAction("LogOut");

            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value);
            string studName;
            using (DbWorker db = new DbWorker())
            {
                var data = db.Query("SELECT FIO FROM students WHERE students_id = \"" + ticket.Name + "\";");
                data.Read();
                studName = data[0].ToString();
            }
            return View(new StringToView(studName));
        }

        [HttpGet]
        public ActionResult NewLab()
        {
            if ((FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value)).UserData != "Student")
                return RedirectToAction("LogOut");

            List<string> vars = new List<string>();
            using (DbWorker db = new DbWorker())
            {
                var data = db.Query("SELECT labNum, variant FROM variants;");
                while (data.Read())
                {
                    vars.Add("Лабораторная №" + data[0].ToString() + " - " + data[1].ToString());
                }
            }
            return View(vars);
        }

        [HttpPost]
        public ActionResult NewLab(string varSelect, string langSelect, HttpPostedFileWrapper fileUpload)
        {
            if ((FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value)).UserData != "Student")
                return RedirectToAction("LogOut");

            if (fileUpload != null)
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value);
                string path = Server.MapPath("/Labs");
                string labName = ticket.Name + "." + varSelect + "." + langSelect;
                fileUpload.SaveAs(Path.Combine(path, labName));
            }
            return RedirectToAction("Index");
        }

        public ActionResult LogOut()
        {
            Response.Cookies["__AUTH_COOK"].Value = "";
            return RedirectToAction("Index", "Home");
        }
    }
}
