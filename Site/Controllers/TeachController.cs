﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using AlgorithmDLL;
using MySql.Data.MySqlClient;
using Site.Models;

namespace Site.Controllers
{
    public class TeachController : Controller
    {
        //
        // GET: /Teach/

        public ActionResult Index()
        {
            FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value);
            if (ticket.UserData != "Teacher")
                return RedirectToAction("LogOut");
            string teachName;
            using (DbWorker db = new DbWorker())
            {
                var data = db.Query("SELECT FIO FROM teachers WHERE teachers_id = \"" + ticket.Name + "\";");
                data.Read();
                teachName = data[0].ToString();
            }
            return View(new StringToView(teachName));
        }

        [HttpGet]
        public ActionResult CheckLabs()
        {
            if ((FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value)).UserData != "Teacher")
                return RedirectToAction("LogOut");

            string path = Server.MapPath("/Labs");
            string[] labsUploaded = Directory.GetFiles(path);
            for (int i = 0; i < labsUploaded.Length; i++)
                labsUploaded[i] = labsUploaded[i].Substring(path.Length + 1);

            List<string[]> table = new List<string[]>();
            using (DbWorker db = new DbWorker())
            {
                foreach (var labName in labsUploaded)
                {
                    var labArray = labName.Split('.');

                    var data = db.Query("SELECT FIO FROM students WHERE students_id = \"" + labArray[0] + "\";");
                    data.Read();
                    labArray[0] = data[0].ToString();
                    data = db.Query("SELECT labNum, variant FROM variants WHERE variants_id = \"" + labArray[1] + "\";");
                    data.Read();
                    labArray[1] = "Лабораторная №" + data[0].ToString() + " - " + data[1].ToString();

                    table.Add(new string[3] { 
                        labArray[0], 
                        labArray[1], 
                        labArray[2] 
                    });
                }
            }
            return View(table);
        }

        [HttpPost]
        public ActionResult CheckLabs(string check)
        {
            if ((FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value)).UserData != "Teacher")
                return RedirectToAction("LogOut");

            string path = Server.MapPath("/Labs");
            string[] labsUploaded = Directory.GetFiles(path);
            List<string> tokens = new List<string>();
            foreach (var lab in labsUploaded)
                tokens.Add((new CodeToken(lab, Server.MapPath("/Labs/languages"), lab.Split('.').Last())).ToString());

            for (int i = 0; i < labsUploaded.Length; i++)
            {
                string[] labInfo = labsUploaded[i].Substring(path.Length + 1).Split('.');
                int tmp, rez = 0;
                string rez_id = "";
                using (DbWorker db = new DbWorker())
                {
                    var dbLabs = db.Query("SELECT * FROM labs WHERE variant = '"+labInfo[1]+"' AND lang = '"+labInfo[2]+"';");
                    if (dbLabs.Read())
                    {
                        do
                        {
                            //tmp = new Random().Next(50, 100);
                            tmp = Algorithms.GST_percent(tokens[i], dbLabs["token"].ToString());
                            if (tmp > rez)
                            {
                                rez = tmp;
                                rez_id = dbLabs["student"].ToString();
                            }
                        } while (dbLabs.Read());

                        if (rez < 70)
                        {
                            db.Query("INSERT INTO `labs` (`student`, `variant`, `lang`, `token`) VALUES ('" + labInfo[0] + "', '" + labInfo[1] + "', '" + labInfo[2] + "', '" + MySqlHelper.EscapeString(tokens.ElementAt(i)) + "');").Read();
                            db.Query("INSERT INTO `last_check` (`student`, `variant`, `added_to_base`, `percent`) VALUES ('" + labInfo[0] + "', '" + labInfo[1] + "', 1, '" + rez.ToString() + "');").Read();
                            System.IO.File.Move(labsUploaded[i], path + "\\archive\\" + (Directory.GetFiles(path + "\\archive\\").Count() + 1).ToString() + "." + labInfo[2]);
                        }
                        else
                        {
                            db.Query("INSERT INTO `last_check` (`student`, `variant`, `added_to_base`, `percent`, `u_kogo_ukradeno`) VALUES ('" + labInfo[0] + "', '" + labInfo[1] + "', 0, '" + rez.ToString() + "', '" + rez_id + "');").Read();
                            System.IO.File.Delete(labsUploaded[i]);
                        }
                    }
                    else
                    {
                        db.Query("INSERT INTO `labs` (`student`, `variant`, `lang`, `token`) VALUES ('"+labInfo[0]+"', '"+labInfo[1]+"', '"+labInfo[2]+"', '"+MySqlHelper.EscapeString(tokens.ElementAt(i))+"');").Read();
                        db.Query("INSERT INTO `last_check` (`student`, `variant`, `added_to_base`, `percent`) VALUES ('" + labInfo[0] + "', '" + labInfo[1] + "', 1, 0);").Read();
                        System.IO.File.Move(labsUploaded[i], path + "\\archive\\" + (Directory.GetFiles(path + "\\archive\\").Count()+1).ToString() + "." + labInfo[2]);
                    }
                }
            }
            return RedirectToAction("LastCheck");
        }

        public ActionResult LastCheck()
        {
            if ((FormsAuthentication.Decrypt(Request.Cookies["__AUTH_COOK"].Value)).UserData != "Teacher")
                return RedirectToAction("LogOut");

            List<string[]> ans = new List<string[]>();
            using(DbWorker db = new DbWorker())
            {
                var data = db.Query("SELECT `students`.`FIO`, `variants`.`labNum`, `variants`.`variant`, `last_check`.`added_to_base`, `last_check`.`percent` "+
                                        "FROM last_check "+
                                        "LEFT JOIN students ON (last_check.student=students.students_id) "+
                                        "LEFT JOIN variants ON (last_check.variant=variants.variants_id) "+
                                        "ORDER BY last_check_id DESC LIMIT 30;"
                );
                while (data.Read())
                {
                    ans.Add(new string[5] {
                        data["FIO"].ToString(),
                        "Л.р. " + data["labNum"].ToString() + " - " + data["variant"].ToString(),
                        (data["added_to_base"].ToString() == "1" ? "Пройдена" : "Не пройдена"),
                        (data["percent"].ToString() != "0" ? data["percent"].ToString()+"%" : "Null"),
                        ""
                    });
                }
                data = db.Query("SELECT `students`.`FIO` FROM last_check LEFT JOIN students ON (last_check.u_kogo_ukradeno=students.students_id) ORDER BY last_check_id DESC LIMIT 30;");
                foreach (var row in ans)
                {
                    data.Read();
                    row[4] = (data["FIO"].ToString() == "" ? "Сам делал" : data["FIO"].ToString());
                }
            }
            return View(ans);
        }

        public ActionResult LogOut()
        {
            Response.Cookies["__AUTH_COOK"].Value = "";
            return RedirectToAction("Index", "Home");
        }
    }
}
