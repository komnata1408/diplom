﻿namespace Site.Models
{
    public class StringToView
    {
        public string message;

        public StringToView(string msg)
        {
            this.message = msg;
        }

        public override string ToString()
        {
            return message;
        }
    }
}