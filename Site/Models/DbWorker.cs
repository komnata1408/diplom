﻿using System;
using MySql.Data.MySqlClient;

namespace Site.Models
{
    public class DbWorker : IDisposable
    {
        private MySqlConnection dbConnHandle;

        public DbWorker()
        {
            this.dbConnHandle = new MySqlConnection("server=188.134.8.12;uid=root;pwd=12345;database=test;");
            this.dbConnHandle.Open();
        }

        public DbWorker(string connectionString)
        {
            this.dbConnHandle = new MySqlConnection(connectionString);
            this.dbConnHandle.Open();
        }

        public MySqlDataReader Query(string query)
        {
            this.dbConnHandle.Dispose();
            this.dbConnHandle.Open();
            MySqlDataReader cmd = new MySqlCommand(query, this.dbConnHandle).ExecuteReader();
            return cmd;
        }

        public void Dispose()
        {
            if (this.dbConnHandle != null)
                dbConnHandle.Close();
        }
    }
}